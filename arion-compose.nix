{ config, pkgs, lib, ... }:

with lib;

let
  secrets = import ./secrets.nix;

  tz = "Europe/London";

  # Common service options
  serviceDefaults = {
    service = {
      environment.TZ = tz;
      restart = "unless-stopped";
      tmpfs = mkDefault [
        "/run"
        "/tmp:exec,mode=777"
      ]; # low priority as NixOS-based containers will override this
    };
    out.service.dns_search = [ "dns.podman" ];
  };

  # Common NixOS-based service options
  nixosServiceDefaults = {
    service = {
      useHostStore = true;
      capabilities.SYS_ADMIN = true; # for nscd
    };
    nixos = {
      useSystemd = true;
      configuration = {
        environment.noXlibs = true;
        boot = {
          tmpOnTmpfs = true;
          isContainer = true;
        };
        time.timeZone = tz;
        # i18n.defaultLocale = "en_GB.UTF-8"; # breaks matrix-db
        system.stateVersion = "21.05";
      };
    };
  };

  mkService = cfg: mkMerge [ serviceDefaults cfg ];

  mkNixosService = cfg: mkMerge [ serviceDefaults nixosServiceDefaults cfg ];

  # Function to generate træfik labels for containers
  traefikLabels = { service, host, port }: {
    "traefik.enable" = "true";
    "traefik.http.routers.${service}.rule" = "Host(`${host}`)";
    "traefik.http.services.${service}.loadbalancer.server.port" = toString port;
    "traefik.http.routers.${service}.tls.certresolver" = "le";
    "traefik.http.routers.${service}.entrypoints" = "websecure";
  };

in {
  config = {
    project.name = "matrix";

    services = {
      nginx = mkNixosService {
        nixos = {
          configuration = {
            services.nginx = {
              enable = true;
              recommendedTlsSettings = true;
              recommendedOptimisation = true;
              recommendedGzipSettings = true;
              recommendedProxySettings = true;
              # user = "root";
              virtualHosts."${secrets.domain}".locations."/".root = "/www";
            };
            systemd.tmpfiles.rules = [
              "L+ /www/.well-known/matrix/server - - - - ${
                builtins.toFile "server" (builtins.toJSON {
                  "m.server" = "synapse.${secrets.domain}:443";
                })
              }"
              "L+ /www/.well-known/matrix/client - - - - ${
                builtins.toFile "client" (builtins.toJSON {
                  "m.homeserver".base_url = "https://${secrets.domain}";
                })
              }"
            ];
          };
        };

        service = rec {
          container_name = "nginx";
          labels = traefikLabels {
            service = container_name;
            host = secrets.domain;
            port = 80;
          };
        };
      };

      matrix-db = mkNixosService {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [ postgresql_13 ];
            services.postgresql = {
              enable = true;
              package = pkgs.postgresql_13;
              enableTCPIP = true;
              port = 5432;
              initialScript = with secrets;
                pkgs.writeText "postgres-init.sql" ''
                  -- Synapse DB
                  CREATE ROLE "${synapse.db.user}" WITH LOGIN PASSWORD '${synapse.db.pass}';
                  CREATE DATABASE "${synapse.db.name}" WITH OWNER "${synapse.db.user}"
                    TEMPLATE template0
                    LC_COLLATE = "C"
                    LC_CTYPE = "C";

                  -- Mautrix-facebook DB
                  CREATE ROLE "${mautrix-facebook.db.user}" WITH LOGIN PASSWORD '${mautrix-facebook.db.pass}';
                  CREATE DATABASE "${mautrix-facebook.db.name}" WITH OWNER "${mautrix-facebook.db.user}"
                    TEMPLATE template0
                    LC_COLLATE = "C"
                    LC_CTYPE = "C";

                  -- Mautrix-twitter DB
                  CREATE ROLE "${mautrix-twitter.db.user}" WITH LOGIN PASSWORD '${mautrix-twitter.db.pass}';
                  CREATE DATABASE "${mautrix-twitter.db.name}" WITH OWNER "${mautrix-twitter.db.user}"
                    TEMPLATE template0
                    LC_COLLATE = "C"
                    LC_CTYPE = "C";

                  -- mx-puppet-slack DB
                  CREATE ROLE "${mx-puppet-slack.db.user}" WITH LOGIN PASSWORD '${mx-puppet-slack.db.pass}';
                  CREATE DATABASE "${mx-puppet-slack.db.name}" WITH OWNER "${mx-puppet-slack.db.user}"
                    TEMPLATE template0
                    LC_COLLATE = "C"
                    LC_CTYPE = "C";

                  -- mx-puppet-skype DB
                  CREATE ROLE "${mx-puppet-skype.db.user}" WITH LOGIN PASSWORD '${mx-puppet-skype.db.pass}';
                  CREATE DATABASE "${mx-puppet-skype.db.name}" WITH OWNER "${mx-puppet-skype.db.user}"
                    TEMPLATE template0
                    LC_COLLATE = "C"
                    LC_CTYPE = "C";
                '';
              authentication = mkForce ''
                # Generated file; do not edit!
                # Trust local connections (IPv4 and IPv6)
                # TYPE  DATABASE        USER            ADDRESS                 METHOD
                local   all             all                                     trust
                host    all             all             127.0.0.1/32            trust
                host    all             all             ::1/128                 trust
                # and those from docker containers
                host    all             all             172.16.0.0/12           trust
                # and those from podman containers
                host    all             all             10.0.0.0/8              trust
              '';
            };
          };
        };

        service = {
          container_name = "matrix-db";
          volumes = [ "${secrets.appdata}/db:/var/lib/postgresql/13" ];
          labels = { "traefik.enable" = "false"; };
        };
      };

      synapse = mkNixosService {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [ matrix-synapse ];
            services.matrix-synapse = {
              dataDir = "/appdata/synapse";
              enable = true;
              server_name = secrets.domain;
              public_baseurl = "https://synapse.${secrets.domain}";
              listeners = [{
                port = 8008;
                type = "http";
                tls = false;
                x_forwarded = true;
                resources = [{
                  names = [ "client" "federation" ];
                  compress = false;
                }];
              }];
              max_upload_size = "100M";
              max_image_pixels = "64M";
              enable_registration = false; # true for open registration
              database_args = with secrets.synapse.db; {
                user = user;
                password = pass;
                database = name;
                host = config.services.matrix-db.service.container_name;
              };
              app_service_config_files = [
                "/appdata/mautrix-facebook/registration.yaml"
                "/appdata/mautrix-twitter/registration.yaml"
                "/appdata/mx-puppet-slack/slack-registration.yaml"
                "/appdata/mx-puppet-skype/skype-registration.yaml"
              ];
              extraConfig = ''
                experimental_features:
                    spaces_enabled: true
                suppress_key_server_warning: true
                trusted_key_servers:
                - server_name: "matrix.org"
                - server_name: "nixos.org"
              '';
            };

            ## Needed when registering new users, see Chap 24 of NixOS manual
            ## TLDR: uncomment lines below and use
            ## docker exec -it synapse register_new_matrix_user -k <registration_shared_secret> http://localhost:8008
            ## or for open registration set enable_registration above to true
            # configuration.environment.systemPackages = [ pkgs.matrix-synapse ];
            # configuration.services.matrix-synapse.registration_shared_secret =
            #   secrets.synapse.registration_shared_secret;
          };
        };

        service = rec {
          container_name = "synapse";
          volumes = [ "${secrets.appdata}:/appdata" ];
          labels = traefikLabels {
            service = container_name;
            host = "synapse.${secrets.domain}";
            port = 8008;
          };
          depends_on = [ "nginx" "matrix-db" ];
        };
      };

      mautrix-facebook = mkService {
        service = {
          container_name = "mautrix-facebook";
          image = "dock.mau.dev/tulir/mautrix-facebook:latest";
          volumes = [ "${secrets.appdata}/mautrix-facebook:/data" ];
          depends_on = [ "synapse" ];
          labels = { "traefik.enable" = "false"; };
        };
      };

      mautrix-twitter = mkService {
        service = {
          container_name = "mautrix-twitter";
          image = "dock.mau.dev/tulir/mautrix-twitter";
          volumes = [ "${secrets.appdata}/mautrix-twitter:/data" ];
          depends_on = [ "synapse" ];
          labels = { "traefik.enable" = "false"; };
        };
      };

      mx-puppet-slack = mkService {
        service = {
          container_name = "mx-puppet-slack";
          image = "sorunome/mx-puppet-slack";
          volumes = [ "${secrets.appdata}/mx-puppet-slack:/data" ];
          depends_on = [ "synapse" ];
          labels = { "traefik.enable" = "false"; };
        };
      };

      mx-puppet-skype = mkService {
        service = {
          container_name = "mx-puppet-skype";
          image = "sorunome/mx-puppet-skype";
          volumes = [ "${secrets.appdata}/mx-puppet-skype:/data" ];
          depends_on = [ "synapse" ];
          labels = { "traefik.enable" = "false"; };
        };
      };
    };
  };
}
